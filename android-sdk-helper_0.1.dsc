-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (native)
Source: android-sdk-helper
Binary: android-sdk-helper
Architecture: all
Version: 0.1
Maintainer: Android Tools Maintainers <android-tools-devel@lists.alioth.debian.org>
Uploaders: Kai-Chung Yan <seamlikok@gmail.com>
Standards-Version: 3.9.8
Vcs-Browser: https://anonscm.debian.org/cgit/android-tools/android-sdk-helper.git
Vcs-Git: https://anonscm.debian.org/git/android-tools/android-sdk-helper.git
Build-Depends: debhelper (>= 10~), default-jdk-headless (>= 2:1.8) | default-jdk (>= 2:1.8), gradle-debian-helper, maven-repo-helper
Package-List:
 android-sdk-helper deb java optional arch=all
Checksums-Sha1:
 57f53a6e55787717b6a8e8fa9c4d7798058abe93 6576 android-sdk-helper_0.1.tar.xz
Checksums-Sha256:
 7e65a494469e3d4748ddd620046e4518837c19cdcdd73d2ccc247e4734699609 6576 android-sdk-helper_0.1.tar.xz
Files:
 88288fa0ac985fa97610ea80b3f14730 6576 android-sdk-helper_0.1.tar.xz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1
Comment: GPG for Android - https://guardianproject.info/code/gnupg/

iQEcBAEBCAAGBQJYwyT9AAoJED4XeBe6G5v6rL8H/1Quo3FJQ8lR8tXwa232WIDT
Ivr3753Wm0DHFFECAdPF+KGCp8t83Ffn8/C81Zm5vx2Od/WpySh/4jC6221V+KCv
Mz0CDvjwhd5NDQsp9tBiauApAD6LUpA1h4GPxGgWyHhjSe+3k9o/HMRbZkCUJ4yC
BgoSJJIDfYCugzyq66OQ7WEE38k0WC2b4kDyM9E+d35q3fS91dOM7k21qn3TPumN
nmdaBPJ5QlWfFuxRGKWfzpG2Ekei9sm5Lqn20/vkyguEoca+o2ha/3U2XyZ14cXe
DcSHSVEML6Xk0XZ2n5SytSu4goMgQFwG7uKJ70EsnpgV8IkPpx5uUe8T93wanT4=
=BK9S
-----END PGP SIGNATURE-----
